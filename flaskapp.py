import os
from flask import Flask
from flask import request, render_template
import pymongo
import json
from bson import json_util
from bson import objectid
from collections import namedtuple
import re

app = Flask(__name__)


@app.route('/')
def home():
    rand_list= [0, 1, 2, 3, 4, 5]
    return render_template('layout.html', a_random_string="Heey, what's up!", a_random_list=rand_list)


@app.route("/doc/get")
def gets():
    #setup the connection
    rand_list= [0, 1, 2, 3, 4, 5]
    conn = pymongo.Connection(os.environ['OPENSHIFT_MONGODB_DB_URL'])
    db = conn[os.environ['OPENSHIFT_APP_NAME']]

    #query the DB for all the parkpoints
    result = db.col_doc.find()

    #Now turn the results into valid JSON
    data = str(json.dumps({'results' : list(result)},default=json_util.default))
    rm_data = data.replace('_id','id')
    doc_data = namedtuple('results', 'id, PID, temperature, pulse, posture')
    j_data = json.loads(rm_data)
    fin_data = [doc_data(**k) for k in j_data["results"]]
    return render_template('result.html', a_random_string="Heey, what's up!", a_random_list=fin_data, d_len=len(fin_data))


@app.route("/doc/put")
def puts():
    #setup the connection
    conn = pymongo.Connection(os.environ['OPENSHIFT_MONGODB_DB_URL'])
    db = conn[os.environ['OPENSHIFT_APP_NAME']]

    pid = str(request.args.get('pid'))
    pulse = str(request.args.get('pulse'))
    temp = str(request.args.get('temp'))
    posture = str(request.args.get('pos'))
    #query the DB for all the parkpoints
    db.col_doc.insert({"PID":pid,"pulse":pulse,"temperature":temp,"posture":posture})
    return "pid="+pid+" pulse="+pulse+" temp="+temp+"pos="+posture


@app.route("/test", methods=['POST'])
def test():
	lat = float(request.form('lat'))
	lon = float(request.form('lon'))		
	return "lat:"+str(lat)+" lon:"+str(lon)

if __name__ == '__main__':
    app.run()
